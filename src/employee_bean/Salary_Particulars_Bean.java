/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package employee_bean;

/**
 *
 * @author Administrator
 */
public class Salary_Particulars_Bean 
{
    private int leaves;
    
    private String month;
    private int year;
    // EARNINGS PART
    private double basic;
    private double hra;
    private double medical;
    private double conveyance;
    private double special;
    private double other;
    private double advance;
    
    // DEDUCTION PART
    private double  professional_tax;
    private double  income_tax;
    private double  epf_amount;
    private double esi_amount;
    private double lwp;
    private double other_dedu;
    
    
    //Calculations part
    private double gross;
    private double net;

    public int getLeaves() {
        return leaves;
    }

    public void setLeaves(int leaves) {
        this.leaves = leaves;
    }   
    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public double getBasic() {
        return basic;
    }

    public void setBasic(double basic) {
        this.basic = basic;
    }

    public double getHra() {
        return hra;
    }

    public void setHra(double hra) {
        this.hra = hra;
    }

    public double getMedical() {
        return medical;
    }

    public void setMedical(double medical) {
        this.medical = medical;
    }

    public double getConveyance() {
        return conveyance;
    }

    public void setConveyance(double conveyance) {
        this.conveyance = conveyance;
    }

    public double getSpecial() {
        return special;
    }

    public void setSpecial(double special) {
        this.special = special;
    }

    public double getOther() {
        return other;
    }

    public void setOther(double other) {
        this.other = other;
    }

    public double getAdvance() {
        return advance;
    }

    public void setAdvance(double advance) {
        this.advance = advance;
    }
    
    

    public double getProfessional_tax() {
        return professional_tax;
    }

    public void setProfessional_tax(double professional_tax) {
        this.professional_tax = professional_tax;
    }

    public double getIncome_tax() {
        return income_tax;
    }

    public void setIncome_tax(double income_tax) {
        this.income_tax = income_tax;
    }

    public double getEpf_amount() {
        return epf_amount;
    }

    public void setEpf_amount(double epf_amount) {
        this.epf_amount = epf_amount;
    }

    public double getEsi_amount() {
        return esi_amount;
    }

    public void setEsi_amount(double esi_amount) {
        this.esi_amount = esi_amount;
    }

    public double getLwp() {
        return lwp;
    }

    public void setLwp(double lwp) {
        this.lwp = lwp;
    }

    public double getOther_dedu() {
        return other_dedu;
    }

    public void setOther_dedu(double other_dedu) {
        this.other_dedu = other_dedu;
    }

    public double getGross() {
        return gross;
    }

    public void setGross(double gross) {
        this.gross = gross;
    }

    public double getNet() {
        return net;
    }

    public void setNet(double net) {
        this.net = net;
    }
    
    
    
}
