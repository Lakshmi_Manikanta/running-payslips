
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.Image;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfGState;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
 
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import sandbox.WrapToTest;
 
@WrapToTest
public class TransparentWatermark3 {
 
    public static final String SRC = "F:\\PaySlip2.pdf";
    public static final String DEST = "F:\\PaySlip2_wm.pdf";
    public static final String IMG = "F:\\getpic.png";
 
    public static void main(String[] args) throws IOException, DocumentException {
        File file = new File(DEST);
        file.getParentFile().mkdirs();
        new TransparentWatermark3().manipulatePdf(SRC, DEST);
    }
 
    public void manipulatePdf(String src, String dest) throws IOException, DocumentException {
        PdfReader reader = new PdfReader(src);
        int n = reader.getNumberOfPages();
        PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(dest));
        stamper.setRotateContents(false);
        // text watermark
        Font f = new Font(FontFamily.TIMES_ROMAN, 30,Font.BOLD);
        Phrase p = new Phrase("Bloom Solutions Pvt Ltd ", f);
        // image watermark
        Image img = Image.getInstance(IMG);
        float w = img.getScaledWidth();
        System .out.println(w);
        float h = img.getScaledHeight();
        System .out.println(h);
        // transparency
        PdfGState gs1 = new PdfGState();
        gs1.setFillOpacity(0.3f);
        // properties
        PdfContentByte over;
        Rectangle pagesize;
        float x, y;
        // loop over every page
        for (int i = 1; i <= n; i++) 
        {
            pagesize = reader.getPageSize(i);
            System .out.println(i);
            x = (pagesize.getLeft() + pagesize.getRight()) / 2;
            y = (pagesize.getTop() + pagesize.getBottom()) / 2;
            over = stamper.getOverContent(i);
            over.saveState();
            over.setGState(gs1);
            if (i % 2 != 1)
                ColumnText.showTextAligned(over, Element.ALIGN_CENTER, p, x, y, 0);
            else
                y=y+30;
                x=x-15;
                //over.addImage(img, w, 0, 0, h, x - (w / 2), y - (h / 80));
                over.addImage(img, w+60, 0, 0, h+60, x - (w / 2), y - (h / 80));
                System .out.println((x - (w / 2))+" ,"+( y - (0)));
                System .out.println((x )+" ,"+( y));
                over.restoreState();
        }
        stamper.close();
        reader.close();
    }
}