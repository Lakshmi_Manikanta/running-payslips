/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package payslip;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPCellEvent;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
 
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import sandbox.WrapToTest;
 

@WrapToTest
public class PaySlip 
{
//    public static final String IMG2 = "F:\\bloom-logo.gif";
    public static final String IMG2 = "F:\\Blm.jpg";
    public static final String IMG1 = "F:\\Rupee.png";
    class RoundRectangle implements PdfPCellEvent 
    {
        public void cellLayout(PdfPCell cell, Rectangle rect,
                PdfContentByte[] canvas) 
        {
            PdfContentByte cb = canvas[PdfPTable.LINECANVAS];
            cb.roundRectangle(
                rect.getLeft() + 1.5f, rect.getBottom() + 1.5f, rect.getWidth() - 3,
                rect.getHeight() - 3, 4);
            cb.stroke();
        }
    }
 
    public static  String DEST = "F:\\PaySlip2.pdf";
 
    public static void main(String[] args) throws IOException, DocumentException
    {
        File file = new File(DEST);
        file.getParentFile().mkdirs();
        new PaySlip().createPdf(DEST);
    }
 
    public void createPdf(String dest) throws IOException, DocumentException {
        //Document document = new Document(PageSize.A4,1.f,1.f,20,1.f);
        Document document = new Document(PageSize.LETTER,1.f,1.f,20,1.f);
        PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(dest));
        writer.setPageEvent(new WatermarkPageEvent());
        document.open();
        PdfPCell cell;
        PdfPCellEvent roundRectangle = new RoundRectangle();
        // outer table
        PdfPTable outertable = new PdfPTable(1);
        // inner table 1
        //PdfPTable innertable = new PdfPTable(5);
        PdfPTable Headertable = new PdfPTable(1);
        Headertable.setWidthPercentage(90);
        
        // first row
        // column 1
    
       
        
       // Headertable.addCell(cell);
        Headertable.addCell(createImageCell(IMG2));
        // Second Row row
        // column 1
        cell = new PdfPCell(font10Bold("PAYSLIP FOR THE MONTH OF MAY 2018"));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBorder(Rectangle.NO_BORDER);  
        cell.setPaddingLeft(2);
        Headertable.addCell(cell);
        
        // Second Row row
        // column 1
         cell = new PdfPCell(font10("H No. 6-2-981, Flat No. 501, 5th Floor, Maruthi Plaza, Khairatabad, Hyderabad - 500004, Telangana."));
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setPaddingLeft(2);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        Headertable.addCell(cell);
        PdfPTable innertable = new PdfPTable(7);
        //innertable.setWidths(new int[]{8, 12, 1, 4, 12});
        innertable.setWidths(new int[]{28,2,34,4,28,2,34});
        outertable.setWidthPercentage(90);
        innertable.setWidthPercentage(90);
  // first row
        // column 1
        cell = new PdfPCell(font10Bold("Employee Name"));
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 2
        cell = new PdfPCell(new Phrase(":"));
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 3
        cell = new PdfPCell(font10("Anumula Lakshmi Manikanta"));
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 4
        cell = new PdfPCell(new Phrase());
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 5
        cell = new PdfPCell(font10Bold("Employee Code"));
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // Coloum 6
        cell = new PdfPCell(new Phrase(":"));
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        //Column 7
        cell = new PdfPCell(font10("BS00145"));
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // spacing
        cell = new PdfPCell();
        cell.setColspan(7);
        cell.setFixedHeight(3);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
  // second row
        // column 1
        cell = new PdfPCell(font10Bold("Designation"));
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 2
        cell = new PdfPCell(new Phrase(":"));
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setPaddingLeft(2);
        innertable.addCell(cell);
        // column 3
        cell = new PdfPCell(font10("Developer"));
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 4
        cell = new PdfPCell(new Phrase());
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setPaddingLeft(5);
        innertable.addCell(cell);
        // column 5
        cell = new PdfPCell(font10Bold("Location"));
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 6
        cell = new PdfPCell(new Phrase(":"));
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
         // column 7
        cell = new PdfPCell(font10("Hyderabad"));
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setPaddingLeft(2);
        innertable.addCell(cell);
        // spacing
        cell = new PdfPCell();
        cell.setColspan(7);
        cell.setFixedHeight(3);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
   // 3rd ROW
         // column 1
        cell = new PdfPCell(font10Bold("Department"));
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 2
        cell = new PdfPCell(new Phrase(":"));
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 3
        cell = new PdfPCell(font10("Design & Development"));
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 4
        cell = new PdfPCell(new Phrase());
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 5
        cell = new PdfPCell(font10Bold("Join Date"));
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // Coloum 6
        cell = new PdfPCell(new Phrase(":"));
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        //Column 7
        cell = new PdfPCell(font10("26-Mar-2014"));
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // spacing
        cell = new PdfPCell();
        cell.setColspan(7);
        cell.setFixedHeight(3);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
   //4th Row
         // column 1
        cell = new PdfPCell(font10Bold("PAN"));
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 2
        cell = new PdfPCell(new Phrase(":"));
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 3
        cell = new PdfPCell(font10("BAMPM6202P"));
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 4
        cell = new PdfPCell(new Phrase());
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 5
        cell = new PdfPCell(font10Bold("PF Account No."));
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // Coloum 6
        cell = new PdfPCell(new Phrase(":"));
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        //Column 7
        cell = new PdfPCell(font10("AP/KKP/123456/123456/123"));
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // spacing
        cell = new PdfPCell();
        cell.setColspan(7);
        cell.setFixedHeight(3);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
   //5th ROW
         // column 1
        cell = new PdfPCell(font10Bold("Bank IFS Code"));
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 2
        cell = new PdfPCell(new Phrase(":"));
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 3
        cell = new PdfPCell(font10("HDFC0002389"));
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 4
        cell = new PdfPCell(new Phrase());
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 5
        cell = new PdfPCell(font10Bold("Bank Account No."));
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // Coloum 6
        cell = new PdfPCell(new Phrase(":"));
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        //Column 7
        cell = new PdfPCell(font10("123456789012345678"));
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // spacing
        cell = new PdfPCell();
        cell.setColspan(7);
        cell.setFixedHeight(3);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);  
   //6th ROW
        // column 1
        cell = new PdfPCell(font10Bold("Bank Name"));
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 2
        cell = new PdfPCell(new Phrase(":"));
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 3
        cell = new PdfPCell(font10("HDFC Bank"));
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 4
        cell = new PdfPCell(new Phrase());
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 5
        cell = new PdfPCell(font10Bold("UAN"));
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // Coloum 6
        cell = new PdfPCell(new Phrase(":"));
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        //Column 7
        cell = new PdfPCell(font10("1234567890123"));
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // spacing
        cell = new PdfPCell();
        cell.setColspan(7);
        cell.setFixedHeight(3);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);       
     //7th ROW
         // column 1
        cell = new PdfPCell(font10Bold("No. Of Working Days"));
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 2
        cell = new PdfPCell(new Phrase(":"));
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 3
        cell = new PdfPCell(font10("30"));
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 4
        cell = new PdfPCell(new Phrase());
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 5
        cell = new PdfPCell(font10Bold("Leaves Taken"));
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // Coloum 6
        cell = new PdfPCell(new Phrase(":"));
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        //Column 7
        cell = new PdfPCell(font10("2"));
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // spacing
        cell = new PdfPCell();
        cell.setColspan(7);
        cell.setFixedHeight(3);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
    //8TH ROW
         // column 1
        cell = new PdfPCell(font10Bold("ESIC No."));
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 2
        cell = new PdfPCell(new Phrase(":"));
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 3
        cell = new PdfPCell(font10("123456789"));
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 4
        cell = new PdfPCell(new Phrase());
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 5
        cell = new PdfPCell(new Phrase());
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // Coloum 6
        cell = new PdfPCell(new Phrase());
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        //Column 7
        cell = new PdfPCell(new Phrase());
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // spacing
        cell = new PdfPCell();
        cell.setColspan(7);
        cell.setFixedHeight(3);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        //------------------------------------------------------------------------------------------
        // first nested table
        cell = new PdfPCell(innertable);
        cell.setCellEvent(roundRectangle);
        //cell.setBorder(Rectangle.NO_BORDER);-----------outer table of first table
        cell.setPadding(8);
        outertable.addCell(Headertable);
        outertable.addCell(cell);

        // inner table 2

        innertable = new PdfPTable(2);
        innertable.setWidths(new int[]{50,50});
        // first row
        // column 1
        
        cell = new PdfPCell(font12Bold("E A R N I N G S"));
       // cell.setBorder(Rectangle.NO_BORDER);
        cell.setMinimumHeight(20);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//        Image img = Image.getInstance(IMG1);
//        img.setWidthPercentage(10);
//        cell.addElement(img);
        innertable.addCell(cell); 
      
        // column 2
        cell = new PdfPCell(font12Bold("D E D U C T I O N S"));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        innertable.addCell(cell);
       
        
        
   // Earnings Table
        PdfPTable  Earningtable = new PdfPTable(2);
        innertable.setWidths(new int[]{50,50});
        // first row
        // column 1
        cell = new PdfPCell(font10Bold("Basic Salary"));
        cell.setBorder(Rectangle.NO_BORDER);
        Earningtable.addCell(cell);
        // column 2
        cell = new PdfPCell(font10("20000"));
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setPaddingRight(20);
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        Earningtable.addCell(cell);
        // spacing
        cell = new PdfPCell(new Phrase());
        cell.setColspan(2);
        cell.setFixedHeight(3);
        cell.setBorder(Rectangle.NO_BORDER);
        Earningtable.addCell(cell);
     // Second row
        // column 1
        cell = new PdfPCell(font10Bold("House Rent Allowance"));
        cell.setBorder(Rectangle.NO_BORDER);
        Earningtable.addCell(cell);
        // column 2
        cell = new PdfPCell(font10("20000"));
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setPaddingRight(20);
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        Earningtable.addCell(cell);
        // spacing
        cell = new PdfPCell(new Phrase());
        cell.setColspan(2);
        cell.setFixedHeight(3);
        cell.setBorder(Rectangle.NO_BORDER);
        Earningtable.addCell(cell);
      // Third row
        // column 1
        cell = new PdfPCell(font10Bold("Medical Allowance"));
        cell.setBorder(Rectangle.NO_BORDER);
        Earningtable.addCell(cell);
        // column 2
        cell = new PdfPCell(font10("20000"));
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setPaddingRight(20);
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        Earningtable.addCell(cell);
        // spacing
        cell = new PdfPCell(new Phrase());
        cell.setColspan(2);
        cell.setFixedHeight(3);
        cell.setBorder(Rectangle.NO_BORDER);
        Earningtable.addCell(cell);
        
    // fourth row
        // column 1
        cell = new PdfPCell(font10Bold("Conveyance Allowance"));
        cell.setBorder(Rectangle.NO_BORDER);
        Earningtable.addCell(cell);
        // column 2
        cell = new PdfPCell(font10("20000"));
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setPaddingRight(20);
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        Earningtable.addCell(cell);
        // spacing
        cell = new PdfPCell(new Phrase());
        cell.setColspan(2);
        cell.setFixedHeight(3);
        cell.setBorder(Rectangle.NO_BORDER);
        Earningtable.addCell(cell);
        
        // Fifth row
        // column 1
        cell = new PdfPCell(font10Bold("Special Allowance"));
        cell.setBorder(Rectangle.NO_BORDER);
        Earningtable.addCell(cell);
        // column 2
        cell = new PdfPCell(font10("20000"));
        cell.setPaddingRight(20);
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setBorder(Rectangle.NO_BORDER);
        Earningtable.addCell(cell);
        // spacing
        cell = new PdfPCell(new Phrase());
        cell.setColspan(2);
        cell.setFixedHeight(3);
        cell.setBorder(Rectangle.NO_BORDER);
        Earningtable.addCell(cell);
        
         // Sixth row
        // column 1
        cell = new PdfPCell(font10Bold("Other Allowance"));
        cell.setBorder(Rectangle.NO_BORDER);
        Earningtable.addCell(cell);
        // column 2
        cell = new PdfPCell(font10("20000"));
        cell.setPaddingRight(20);
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setBorder(Rectangle.NO_BORDER);
        Earningtable.addCell(cell);
        // spacing
        cell = new PdfPCell(new Phrase());
        cell.setColspan(2);
        cell.setFixedHeight(3);
        cell.setBorder(Rectangle.NO_BORDER);
        Earningtable.addCell(cell);
    // Deductions Table
        PdfPTable  Deductions = new PdfPTable(2);
        innertable.setWidths(new int[]{50,50});
        // first row
        // column 1
        cell = new PdfPCell(font10Bold("Income Tax"));
        cell.setBorder(Rectangle.NO_BORDER);
        Deductions.addCell(cell);
        // column 2
        cell = new PdfPCell(font10("20000"));
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setPaddingRight(20);
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        Deductions.addCell(cell);
        // spacing
        cell = new PdfPCell(new Phrase());
        cell.setColspan(2);
        cell.setFixedHeight(3);
        cell.setBorder(Rectangle.NO_BORDER);
        Deductions.addCell(cell);
        
        // Second row
        // column 1
        cell = new PdfPCell(font10Bold("Profession Tax"));
        cell.setBorder(Rectangle.NO_BORDER);
        Deductions.addCell(cell);
        // column 2
        cell = new PdfPCell(font10("20000"));
        cell.setPaddingRight(20);
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setBorder(Rectangle.NO_BORDER);
        Deductions.addCell(cell);
        // spacing
        cell = new PdfPCell(new Phrase());
        cell.setColspan(2);
        cell.setFixedHeight(3);
        cell.setBorder(Rectangle.NO_BORDER);
        Deductions.addCell(cell);
        
         // Third Row
        // column 1
        cell = new PdfPCell(font10Bold("EPF "));
        cell.setBorder(Rectangle.NO_BORDER);
        Deductions.addCell(cell);
        // column 2
        cell = new PdfPCell(font10("20000"));
        cell.setPaddingRight(20);
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setBorder(Rectangle.NO_BORDER);
        Deductions.addCell(cell);
        // spacing
        cell = new PdfPCell(new Phrase());
        cell.setColspan(2);
        cell.setFixedHeight(3);
        cell.setBorder(Rectangle.NO_BORDER);
        Deductions.addCell(cell);
        
         // Fourth Row
        // column 1
        cell = new PdfPCell(font10Bold("ESI "));
        cell.setBorder(Rectangle.NO_BORDER);
        Deductions.addCell(cell);
        // column 2
        cell = new PdfPCell(font10("20000"));
        cell.setPaddingRight(20);
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setBorder(Rectangle.NO_BORDER);
        Deductions.addCell(cell);
        // spacing
        cell = new PdfPCell(new Phrase());
        cell.setColspan(2);
        cell.setFixedHeight(3);
        cell.setBorder(Rectangle.NO_BORDER);
        Deductions.addCell(cell);
        
         // fifth Row
        // column 1
        cell = new PdfPCell(font10Bold("LWP "));
        cell.setBorder(Rectangle.NO_BORDER);
        Deductions.addCell(cell);
        // column 2
        cell = new PdfPCell(font10("20000"));
        cell.setPaddingRight(20);
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setBorder(Rectangle.NO_BORDER);
        Deductions.addCell(cell);
        // spacing
        cell = new PdfPCell(new Phrase());
        cell.setColspan(2);
        cell.setFixedHeight(3);
        cell.setBorder(Rectangle.NO_BORDER);
        Deductions.addCell(cell);
        
        // Sixth Row
        // column 1
        cell = new PdfPCell(font10Bold("Other Deductions "));
        cell.setBorder(Rectangle.NO_BORDER);
        Deductions.addCell(cell);
        // column 2
        cell = new PdfPCell(font10("20000"));
        cell.setPaddingRight(20);
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setBorder(Rectangle.NO_BORDER);
        Deductions.addCell(cell);
        // spacing
        cell = new PdfPCell(new Phrase());
        cell.setColspan(2);
        cell.setFixedHeight(3);
        cell.setBorder(Rectangle.NO_BORDER);
        Deductions.addCell(cell);
      // Gross Total Table
        PdfPTable  Total = new PdfPTable(2);
        innertable.setWidths(new int[]{50,50});
        // first row
        // column 1
        cell = new PdfPCell(font10Bold("CTC"));
        cell.setBorder(Rectangle.NO_BORDER);
        Total.addCell(cell);
        // column 2
        cell = new PdfPCell(font12Bold("20000"));
        cell.setPaddingRight(20);
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setBorder(Rectangle.NO_BORDER);
        Total.addCell(cell);
        // spacing
        cell = new PdfPCell(new Phrase());
        cell.setColspan(2);
        cell.setFixedHeight(3);
        cell.setBorder(Rectangle.NO_BORDER);
        Total.addCell(cell);
        
         // Deduction Total Table
        PdfPTable  DedTotal = new PdfPTable(2);
        innertable.setWidths(new int[]{50,50});
        // first row
        // column 1
        cell = new PdfPCell(font10Bold("Total Deduction"));
        cell.setBorder(Rectangle.NO_BORDER);
        DedTotal.addCell(cell);
        // column 2
        cell = new PdfPCell(font12Bold("20000"));
        cell.setPaddingRight(20);
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setBorder(Rectangle.NO_BORDER);
        DedTotal.addCell(cell);
        // spacing
        cell = new PdfPCell(new Phrase());
        cell.setColspan(2);
        cell.setFixedHeight(3);
        cell.setBorder(Rectangle.NO_BORDER);
        DedTotal.addCell(cell);
        
        innertable.addCell(Earningtable);
        innertable.addCell(Deductions);
        innertable.addCell(Total);
        innertable.addCell(DedTotal);
        
        // spacing
        cell = new PdfPCell(new Phrase("Space"));
        cell.setColspan(4);
        cell.setFixedHeight(3);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
    
        // second nested table
        cell = new PdfPCell(innertable);
        cell.setCellEvent(roundRectangle);
        //cell.setBorder(Rectangle.NO_BORDER);
        cell.setPadding(8);
        outertable.addCell(cell);
        // add the table
        
        
        PdfPTable outtable = new PdfPTable(4);
        outtable.setWidths(new int[]{12,3,17,85});
        outtable.setWidthPercentage(90);
        outtable.setSpacingBefore(20f);
        
        // first row
        // column 1
        cell = new PdfPCell(font12Bold("Net Pay :"));
        cell.setPaddingRight(5);
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
       cell.setBorder(Rectangle.NO_BORDER);
        outtable.addCell(cell);
        
        //column 2
         outtable.addCell(RupeeImageCell(IMG1));
         
        // column 3  ;
        Chunk chunk1 = new Chunk("0123456789");
        chunk1.setUnderline(1.5f, -4);
        cell = new PdfPCell(new Phrase(chunk1)); 
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBorder(Rectangle.NO_BORDER);
        outtable.addCell(cell);
        
        // column 4
        //cell = new PdfPCell(font8Bold("( Seventy Seven Lakh+s Seventy Seven Thousand Seven Hundred Seventy Seven )"));
        cell = new PdfPCell(font8Bold("( Rupees Seven Lakhs Seven Thousand Seven Hundred Seventy Seven Only )"));
        cell.setPaddingRight(5);
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        cell.setBorder(Rectangle.NO_BORDER);
        outtable.addCell(cell);
        // spacing
        cell = new PdfPCell();
        cell.setColspan(4);
        cell.setFixedHeight(3);
        cell.setBorder(Rectangle.NO_BORDER);
        outtable.addCell(cell);
        
        document.add(outertable);
        document.add(outtable);
        Paragraph dummy = new Paragraph("\u00a0");
        Paragraph dummy2 = new Paragraph("");
       // document.add(dummy);
       // document.add(Phrase.getInstance(2, "           In Words : Rupess Sixty seven thousand two hundred sixty Three only ", new Font(Font.FontFamily.TIMES_ROMAN, 12))); 
        document.add(dummy2);
        document.add(Phrase.getInstance(2, "                 Note : Since this a computer - generated statement, it does not need any signature.", new Font(Font.FontFamily.TIMES_ROMAN, 8)));
        document.close();
    }
    
     public static Phrase font8Bold( String phr)
    {
        Phrase pr = new Phrase(Phrase.getInstance(0, phr, new Font(Font.FontFamily.TIMES_ROMAN, 9,Font.BOLD)));
        return pr;
    }
    public static Phrase font10( String phr)
    {
        Phrase pr = new Phrase(Phrase.getInstance(10, phr, new Font(Font.FontFamily.TIMES_ROMAN, 10)));
        
        return pr;
    }
     public static Phrase font10Bold( String phr)
    {
        Phrase pr = new Phrase(Phrase.getInstance(0, phr, new Font(Font.FontFamily.TIMES_ROMAN, 10,Font.BOLD)));
        return pr;
    }
      public static Paragraph font10R( String phr)
    {
        
       Paragraph info = new Paragraph(phr);
       info.setAlignment(Element.ALIGN_CENTER);      
        return info;
    }
     
     public static Phrase font12Bold( String phr)
    {
        Phrase pr = new Phrase(Phrase.getInstance(0, phr, new Font(Font.FontFamily.TIMES_ROMAN, 12,Font.BOLD)));
        return pr;
    }
     
      public static PdfPCell createImageCell(String path) throws DocumentException, IOException 
      {
        Image img = Image.getInstance(path);
       // img.setWidthPercentage(5);
        PdfPCell cell = new PdfPCell(img, true);
        cell.setFixedHeight(50);
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setPaddingLeft(2);
        return cell;
      }
      public static PdfPCell RupeeImageCell(String path) throws DocumentException, IOException 
      {
        Image img = Image.getInstance(path);       
        PdfPCell cell = new PdfPCell(img, true);
        cell.setFixedHeight(5);
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);       
        cell.setPaddingTop(3);
        return cell;
      }
}