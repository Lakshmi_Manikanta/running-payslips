/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package payslip;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPCellEvent;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
 
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
//import sandbox.WrapToTest;
 

//@WrapToTest
public class PaySlip_Basic 
{
    class RoundRectangle implements PdfPCellEvent 
    {
        public void cellLayout(PdfPCell cell, Rectangle rect,
                PdfContentByte[] canvas) 
        {
            PdfContentByte cb = canvas[PdfPTable.LINECANVAS];
            cb.roundRectangle(
                rect.getLeft() + 1.5f, rect.getBottom() + 1.5f, rect.getWidth() - 3,
                rect.getHeight() - 3, 4);
            cb.stroke();
        }
    }
 
    public static  String DEST = "F:\\qw_PaySlip.pdf";
 
    public static void main(String[] args) throws IOException, DocumentException {
        File file = new File(DEST);
        file.getParentFile().mkdirs();
        new PaySlip_Basic().createPdf(DEST);
    }
 
    public void createPdf(String dest) throws IOException, DocumentException {
        Document document = new Document(PageSize.A4,1.f,1.f,20,1.f);
        PdfWriter.getInstance(document, new FileOutputStream(dest));
        document.open();
        PdfPCell cell;
        PdfPCellEvent roundRectangle = new RoundRectangle();
        // outer table
        PdfPTable outertable = new PdfPTable(1);
        // inner table 1
        //PdfPTable innertable = new PdfPTable(5);
        PdfPTable innertable = new PdfPTable(7);
        //innertable.setWidths(new int[]{8, 12, 1, 4, 12});
        innertable.setWidths(new int[]{28,2,34,4,28,2,34});
        outertable.setWidthPercentage(90);
        innertable.setWidthPercentage(90);
  // first row
        // column 1
        cell = new PdfPCell(font10Bold("Employee Name"));
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 2
        cell = new PdfPCell(new Phrase(":"));
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 3
        cell = new PdfPCell(font10("Manikanta"));
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 4
        cell = new PdfPCell(new Phrase());
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 5
        cell = new PdfPCell(font10Bold("Employee Code"));
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // Coloum 6
        cell = new PdfPCell(new Phrase(":"));
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        //Column 7
        cell = new PdfPCell(font10("BS00145"));
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // spacing
        cell = new PdfPCell();
        cell.setColspan(7);
        cell.setFixedHeight(3);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
  // second row
        // column 1
        cell = new PdfPCell(font10Bold("Designation"));
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 2
        cell = new PdfPCell(new Phrase(":"));
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setPaddingLeft(2);
        innertable.addCell(cell);
        // column 3
        cell = new PdfPCell(font10("Developer"));
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 4
        cell = new PdfPCell(new Phrase());
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setPaddingLeft(5);
        innertable.addCell(cell);
        // column 5
        cell = new PdfPCell(font10Bold("Location"));
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 6
        cell = new PdfPCell(new Phrase(":"));
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
         // column 7
        cell = new PdfPCell(font10("Hyderabad"));
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setPaddingLeft(2);
        innertable.addCell(cell);
        // spacing
        cell = new PdfPCell();
        cell.setColspan(7);
        cell.setFixedHeight(3);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
   // 3rd ROW
         // column 1
        cell = new PdfPCell(font10Bold("Department"));
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 2
        cell = new PdfPCell(new Phrase(":"));
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 3
        cell = new PdfPCell(font10("Design & Development"));
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 4
        cell = new PdfPCell(new Phrase());
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 5
        cell = new PdfPCell(font10Bold("Join Date"));
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // Coloum 6
        cell = new PdfPCell(new Phrase(":"));
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        //Column 7
        cell = new PdfPCell(font10("26-Mar-2014"));
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // spacing
        cell = new PdfPCell();
        cell.setColspan(7);
        cell.setFixedHeight(3);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
   //4th Row
         // column 1
        cell = new PdfPCell(font10Bold("PAN Number"));
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 2
        cell = new PdfPCell(new Phrase(":"));
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 3
        cell = new PdfPCell(font10("BAMPM6202P"));
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 4
        cell = new PdfPCell(new Phrase());
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 5
        cell = new PdfPCell(font10Bold("PF Account No."));
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // Coloum 6
        cell = new PdfPCell(new Phrase(":"));
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        //Column 7
        cell = new PdfPCell(font10("AP/KKP/123456/123456/123"));
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // spacing
        cell = new PdfPCell();
        cell.setColspan(7);
        cell.setFixedHeight(3);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
   //5th ROW
         // column 1
        cell = new PdfPCell(font10Bold("Bank IFSC Code"));
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 2
        cell = new PdfPCell(new Phrase(":"));
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 3
        cell = new PdfPCell(font10("HDFC0002389"));
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 4
        cell = new PdfPCell(new Phrase());
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 5
        cell = new PdfPCell(font10Bold("Bank Account No."));
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // Coloum 6
        cell = new PdfPCell(new Phrase(":"));
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        //Column 7
        cell = new PdfPCell(font10("123456789012345678"));
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // spacing
        cell = new PdfPCell();
        cell.setColspan(7);
        cell.setFixedHeight(3);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);  
   //6th ROW
        // column 1
        cell = new PdfPCell(font10Bold("Bank Name"));
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 2
        cell = new PdfPCell(new Phrase(":"));
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 3
        cell = new PdfPCell(font10("HDFC Bank"));
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 4
        cell = new PdfPCell(new Phrase());
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 5
        cell = new PdfPCell(font10Bold("UAN No."));
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // Coloum 6
        cell = new PdfPCell(new Phrase(":"));
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        //Column 7
        cell = new PdfPCell(font10("1234567890123"));
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // spacing
        cell = new PdfPCell();
        cell.setColspan(7);
        cell.setFixedHeight(3);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);       
     //7th ROW
         // column 1
        cell = new PdfPCell(font10Bold("No. Of Working Days"));
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 2
        cell = new PdfPCell(new Phrase(":"));
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 3
        cell = new PdfPCell(font10("30"));
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 4
        cell = new PdfPCell(new Phrase());
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 5
        cell = new PdfPCell(font10Bold("Leaves Taken"));
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // Coloum 6
        cell = new PdfPCell(new Phrase(":"));
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        //Column 7
        cell = new PdfPCell(font10("2"));
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // spacing
        cell = new PdfPCell();
        cell.setColspan(7);
        cell.setFixedHeight(3);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
    //8TH ROW
         // column 1
        cell = new PdfPCell(font10Bold("ESCI No."));
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 2
        cell = new PdfPCell(new Phrase(":"));
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 3
        cell = new PdfPCell(font10("123456789"));
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 4
        cell = new PdfPCell(new Phrase());
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 5
        cell = new PdfPCell(new Phrase());
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // Coloum 6
        cell = new PdfPCell(new Phrase());
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        //Column 7
        cell = new PdfPCell(new Phrase());
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // spacing
        cell = new PdfPCell();
        cell.setColspan(7);
        cell.setFixedHeight(3);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        //------------------------------------------------------------------------------------------
        // first nested table
        cell = new PdfPCell(innertable);
        cell.setCellEvent(roundRectangle);
       // cell.setBorder(Rectangle.NO_BORDER);-----------outer table of first table
        cell.setPadding(8);
        outertable.addCell(cell);
        
        
        
        // inner table 2
        innertable = new PdfPTable(4);
        innertable.setWidths(new int[]{3, 17, 1, 16});
        // first row
        // column 1
        cell = new PdfPCell(new Phrase("Blank 1"));
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 2
        cell = new PdfPCell(new Phrase("Name"));
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 3
        cell = new PdfPCell(new Phrase("Blank2"));
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 4
        cell = new PdfPCell(new Phrase("Signature: "));
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // spacing
        cell = new PdfPCell(new Phrase("Space"));
        cell.setColspan(4);
        cell.setFixedHeight(3);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // subsequent rows
        for (int i = 1; i <= 4; i++) {
            // column 1
            cell = new PdfPCell(new Phrase(String.format("%s:", i)));
            cell.setBorder(Rectangle.NO_BORDER);
            innertable.addCell(cell);
            // column 2
            cell = new PdfPCell(new Phrase("Mani "));
            innertable.addCell(cell);
            // column 3
            cell = new PdfPCell(new Phrase("MAnni2"));
            cell.setBorder(Rectangle.NO_BORDER);
            innertable.addCell(cell);
            // column 4
            cell = new PdfPCell(new Phrase("Mani3"));
            innertable.addCell(cell);
            // spacing
            cell = new PdfPCell();
            cell.setColspan(4);
            cell.setFixedHeight(3);
            cell.setBorder(Rectangle.NO_BORDER);
            innertable.addCell(cell);
        }
        // second nested table
        cell = new PdfPCell(innertable);
        cell.setCellEvent(roundRectangle);
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setPadding(8);
        outertable.addCell(cell);
        // add the table
        document.add(outertable);
        document.close();
    }
    
    public static Phrase font10( String phr)
    {
        Phrase pr = new Phrase(Phrase.getInstance(10, phr, new Font(Font.FontFamily.TIMES_ROMAN, 10)));
        return pr;
    }
     public static Phrase font10Bold( String phr)
    {
        Phrase pr = new Phrase(Phrase.getInstance(0, phr, new Font(Font.FontFamily.TIMES_ROMAN, 10,Font.BOLD)));
        return pr;
    }
}