package payslip;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPCellEvent;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import employee_bean.Information_Bean;
import employee_bean.Salary_Particulars_Bean;
 
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import sandbox.WrapToTest;
 

@WrapToTest
public class PaySlip_newDB_mail 
{
//    public static final String IMG2 = "F:\\bloom-logo.gif";
    public static final String IMG2 = "D:\\F drive BK\\Blm.jpg";
    public static final String IMG1 = "D:\\F drive BK\\Rupee.png";
    class RoundRectangle implements PdfPCellEvent 
    {
        public void cellLayout(PdfPCell cell, Rectangle rect,
                PdfContentByte[] canvas) 
        {
            PdfContentByte cb = canvas[PdfPTable.LINECANVAS];
            cb.roundRectangle(
                rect.getLeft() + 1.5f, rect.getBottom() + 1.5f, rect.getWidth() - 3,
                rect.getHeight() - 3, 4);
            cb.stroke();
        }
    }
 
    public static  String DEST = "F:\\Created PDfs\\PaySlip_DB.pdf";
 
    public static void main(String[] args) throws IOException, DocumentException, SQLException, Exception
    {
        Connection con=null;
        File file = new File(DEST);
        file.getParentFile().mkdirs();
        Information_Bean  info=new Information_Bean ();
        Salary_Particulars_Bean sal= new Salary_Particulars_Bean();
        PaySlip_newDB_mail obj= new PaySlip_newDB_mail();
        con=obj.getConnection();
        String qry_ind=" SELECT A.EMP_ID,A.EMP_SUR_NAME,A.EMP_NAME,A.EMP_DEP,A.EMP_DESG,A.EMP_WORK_LOCATION,\n" +
" A.EMP_MAIL_ID,A.EMP_IFSC_CODE,A.EMP_ESI_NO,A.EMP_BANK_ACNO,A.EMP_BANK_NAME,A.EMP_PAN_NO,A.EMP_EPF_NO, A.EMP_DATE_OF_JOIN,A.EMP_EPFUAN_NO,\n" +
" B.BASIC,B.HRA,B.CONVEYANCE,B.LEAVE_DEDUCTIONS,B.LEAVES,B.MEDICAL,B.EOTHERS,B.ADMIN_CHARGES,B.INCENTIVE,B.PF_EMPLOYER_CONTRIBUTION,b.PF_EMPLOYEE_CONTRIBUTION,\n" +
" B.ADVANCES_OTHER_DEDUCTIONS,B.PT,B.TDS,B.MONTH,B.YEAR,B.CTC2,B.NET_PAY,b.ESI_EMPLOYEE_CONTRIBUTION,b.ESI_EMPLOYER_CONTRIBUTION\n" +
" FROM bloom_emp_master_08082018 A,bloom_sal_details_2018  B WHERE A.EMP_ID=B.EMPLOYEE_ID --and A.EMP_ID='BS00182'";      
        PreparedStatement stmt=con.prepareStatement(qry_ind);  
        ResultSet rs=stmt.executeQuery();  
        while(rs.next())
        {  
            //Employee Information
            
                info.setE_code(rs.getString("EMP_ID")!=null?rs.getString("EMP_ID"):"NA") ;
                info.setE_name(rs.getString("EMP_SUR_NAME")!=null && rs.getString("EMP_NAME")!=null?rs.getString("EMP_SUR_NAME")+" "+rs.getString("EMP_NAME"):"NA");             
                info.setLocation(rs.getString("EMP_WORK_LOCATION")!=null?rs.getString("EMP_WORK_LOCATION"):"NA");
                info.setDesignation(rs.getString("EMP_DESG")!=null?rs.getString("EMP_DESG"):"");            
                info.setE_mail(rs.getString("EMP_MAIL_ID"));
                if (rs.wasNull()){info.setE_mail("-");}
                info.setAcc_no(rs.getString("EMP_BANK_ACNO")!=null?rs.getString("EMP_BANK_ACNO"):"NA");
                info.setBname(rs.getString("EMP_BANK_NAME")!=null?rs.getString("EMP_BANK_NAME"):"NA");
                info.setIfsc(rs.getString("EMP_IFSC_CODE")!=null?rs.getString("EMP_IFSC_CODE"):"NA");
                info.setPan(rs.getString("EMP_PAN_NO")!=null && !rs.getString("EMP_PAN_NO").equals("null") && !rs.getString("EMP_PAN_NO").trim().equals("")?rs.getString("EMP_PAN_NO"):"NA");
                info.setPf(rs.getString("EMP_EPF_NO")!=null?rs.getString("EMP_EPF_NO"):"NA");
                info.setLeaves(rs.getInt("LEAVES")!=0?rs.getInt("LEAVES"):0);
                info.setEsi(rs.getString("EMP_ESI_NO")!=null?rs.getString("EMP_ESI_NO"):"NA");
                // Hard coded values
                info.setDepartmant(rs.getString("EMP_DEP")!=null?rs.getString("EMP_DEP"):"NA");//EMP_DEPARTMENT
                info.setJoin_dt(rs.getString("EMP_DATE_OF_JOIN")!=null?(rs.getString("EMP_DATE_OF_JOIN")):"NA");
                info.setUan(rs.getString("EMP_EPFUAN_NO")!=null?rs.getString("EMP_EPFUAN_NO"):"NA");
             //Employee Salary particulars.
                sal.setMonth(rs.getString("MONTH"));               
                sal.setYear(rs.getInt("YEAR"));
                sal.setEpf_amount(rs.getInt("PF_EMPLOYER_CONTRIBUTION")!=0 && rs.getInt("PF_EMPLOYEE_CONTRIBUTION")!=0?rs.getInt("PF_EMPLOYER_CONTRIBUTION")+rs.getInt("PF_EMPLOYEE_CONTRIBUTION")+rs.getInt("ADMIN_CHARGES"):0);
                sal.setBasic(rs.getInt("BASIC")!=0?rs.getInt("BASIC"):0);
                sal.setHra(rs.getInt("HRA")!=0?rs.getInt("HRA"):0);
                sal.setConveyance(rs.getInt("CONVEYANCE")!=0?rs.getInt("CONVEYANCE"):0);
                sal.setMedical(rs.getInt("MEDICAL")!=0?rs.getInt("MEDICAL"):0);
                sal.setSpecial(rs.getInt("EOTHERS")!=0?rs.getInt("EOTHERS"):0);
                sal.setOther(rs.getInt("ESI_EMPLOYER_CONTRIBUTION")!=0 ||rs.getInt("PF_EMPLOYEE_CONTRIBUTION")!=0?rs.getInt("PF_EMPLOYEE_CONTRIBUTION")+rs.getInt("ESI_EMPLOYER_CONTRIBUTION")+rs.getInt("ADMIN_CHARGES")+rs.getInt("INCENTIVE"):0);
                sal.setLwp(rs.getInt("LEAVE_DEDUCTIONS")!=0?rs.getInt("LEAVE_DEDUCTIONS"):0);
                sal.setAdvance(rs.getInt("ADVANCES_OTHER_DEDUCTIONS")!=0?rs.getInt("ADVANCES_OTHER_DEDUCTIONS"):0);
                sal.setProfessional_tax(rs.getInt("PT")!=0?rs.getInt("PT"):0);
                
                sal.setIncome_tax(rs.getInt("TDS")!=0?rs.getInt("TDS"):0);
                sal.setEsi_amount(rs.getInt("ESI_EMPLOYEE_CONTRIBUTION")!=0 && rs.getInt("ESI_EMPLOYER_CONTRIBUTION")!=0?rs.getInt("ESI_EMPLOYER_CONTRIBUTION")+rs.getInt("ESI_EMPLOYEE_CONTRIBUTION"):0);
                sal.setGross(rs.getInt("CTC2")!=0?rs.getInt("CTC2")+rs.getInt("INCENTIVE"):0);
                sal.setNet(rs.getInt("NET_PAY")!=0?rs.getInt("NET_PAY"):0);
                
                info.setSal_bean(sal);
                String path="F:\\Created PDfs\\PaySlip_DB_"+info.getE_code()+"_"+info.getSal_bean().getMonth()+".pdf";
                obj.createPdf(path,info);
               // System.out.println("One file generated");
                 Mailer.sendTO(info, path);
                
//                Thread t=new Thread(Mailer.sendTO(info, path));
//                t.start();
        }
        con.close();
        rs.close();
        stmt.close();
        
    }
    
    public Connection getConnection()
    { 
        Connection connection = null;
        try
        {
                       
            Class.forName("oracle.jdbc.driver.OracleDriver");
            connection=DriverManager.getConnection("jdbc:oracle:thin:@192.168.200.45:1521:orcl","system","oracle");
            System.out.println("Connected to_" + connection); 
        } 
        catch(Exception e)
        {
            e.printStackTrace();
           
        }
        return connection;
    }
 
    public void createPdf(String dest,Information_Bean info) throws IOException, DocumentException, Exception {
        //Document document = new Document(PageSize.A4,1.f,1.f,20,1.f);
        Document document = new Document(PageSize.LETTER,1.f,1.f,20,1.f);
        PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(dest));
        writer.setPageEvent(new WatermarkPageEvent());
        document.open();
        PdfPCell cell;
        PdfPCellEvent roundRectangle = new RoundRectangle();
        // outer table
        PdfPTable outertable = new PdfPTable(1);
        // inner table 1
        //PdfPTable innertable = new PdfPTable(5);
        PdfPTable Headertable = new PdfPTable(1);
        Headertable.setWidthPercentage(90);
        
        // first row
        // column 1
    
       
        
       // Headertable.addCell(cell);
        Headertable.addCell(createImageCell(IMG2));
        // Second Row row
        // column 1
        cell = new PdfPCell(font10Bold("PAYSLIP FOR THE MONTH OF "+info.getSal_bean().getMonth().toUpperCase()+" "+info.getSal_bean().getYear()+""));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBorder(Rectangle.NO_BORDER);  
        cell.setPaddingLeft(2);
        Headertable.addCell(cell);
        
        // Second Row row
        // column 1
         cell = new PdfPCell(font10("H No. 6-2-981, Flat No. 501, 5th Floor, Maruthi Plaza, Khairatabad - 500004, Hyderabad, Telangana."));
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setPaddingLeft(2);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        Headertable.addCell(cell);
        PdfPTable innertable = new PdfPTable(7);
        //innertable.setWidths(new int[]{8, 12, 1, 4, 12});
        innertable.setWidths(new int[]{28,2,34,4,28,2,34});
        outertable.setWidthPercentage(90);
        innertable.setWidthPercentage(90);
  // first row
        // column 1
        cell = new PdfPCell(font10Bold("Employee Name"));
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 2
        cell = new PdfPCell(new Phrase(":"));
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 3
        cell = new PdfPCell(font10(info.getE_name()));
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 4
        cell = new PdfPCell(new Phrase());
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 5
        cell = new PdfPCell(font10Bold("Employee Code"));
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // Coloum 6
        cell = new PdfPCell(new Phrase(":"));
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        //Column 7
        cell = new PdfPCell(font10(info.getE_code()));
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // spacing
        cell = new PdfPCell();
        cell.setColspan(7);
        cell.setFixedHeight(3);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
  // second row
        // column 1
        cell = new PdfPCell(font10Bold("Designation"));
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 2
        cell = new PdfPCell(new Phrase(":"));
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setPaddingLeft(2);
        innertable.addCell(cell);
        // column 3
        cell = new PdfPCell(font10(info.getDesignation()));
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 4
        cell = new PdfPCell(new Phrase());
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setPaddingLeft(5);
        innertable.addCell(cell);
        // column 5
        cell = new PdfPCell(font10Bold("Location"));
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 6
        cell = new PdfPCell(new Phrase(":"));
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
         // column 7
        cell = new PdfPCell(font10(info.getLocation()));
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setPaddingLeft(2);
        innertable.addCell(cell);
        // spacing
        cell = new PdfPCell();
        cell.setColspan(7);
        cell.setFixedHeight(3);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
   // 3rd ROW
         // column 1
        cell = new PdfPCell(font10Bold("Department"));
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 2
        cell = new PdfPCell(new Phrase(":"));
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 3
        cell = new PdfPCell(font10(info.getDepartmant()));
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 4
        cell = new PdfPCell(new Phrase());
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 5
        cell = new PdfPCell(font10Bold("Join Date"));
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // Coloum 6
        cell = new PdfPCell(new Phrase(":"));
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        //Column 7
        
       // cell = new PdfPCell(font10(new SimpleDateFormat("dd-mmm-yyyy").format(info.getJoin_dt())));
       cell = new PdfPCell(font10((info.getJoin_dt())));
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // spacing
        cell = new PdfPCell();
        cell.setColspan(7);
        cell.setFixedHeight(3);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
   //4th Row
         // column 1
        cell = new PdfPCell(font10Bold("PAN"));
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 2
        cell = new PdfPCell(new Phrase(":"));
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 3
        cell = new PdfPCell(font10(info.getPan()));
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 4
        cell = new PdfPCell(new Phrase());
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 5
        cell = new PdfPCell(font10Bold("PF Account No."));
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // Coloum 6
        cell = new PdfPCell(new Phrase(":"));
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        //Column 7
        cell = new PdfPCell(font10(info.getPf()));
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // spacing
        cell = new PdfPCell();
        cell.setColspan(7);
        cell.setFixedHeight(3);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
   //5th ROW
         // column 1
        cell = new PdfPCell(font10Bold("Bank IFS Code"));
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 2
        cell = new PdfPCell(new Phrase(":"));
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 3
        cell = new PdfPCell(font10(info.getIfsc()));
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 4
        cell = new PdfPCell(new Phrase());
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 5
        cell = new PdfPCell(font10Bold("Bank Account No."));
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // Coloum 6
        cell = new PdfPCell(new Phrase(":"));
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        //Column 7
        cell = new PdfPCell(font10(info.getAcc_no()));
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // spacing
        cell = new PdfPCell();
        cell.setColspan(7);
        cell.setFixedHeight(3);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);  
   //6th ROW
        // column 1
        cell = new PdfPCell(font10Bold("Bank Name"));
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 2
        cell = new PdfPCell(new Phrase(":"));
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 3
        cell = new PdfPCell(font10(info.getBname()));
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 4
        cell = new PdfPCell(new Phrase());
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 5
        cell = new PdfPCell(font10Bold("UAN"));
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // Coloum 6
        cell = new PdfPCell(new Phrase(":"));
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        //Column 7
        cell = new PdfPCell(font10(info.getUan()));
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // spacing
        cell = new PdfPCell();
        cell.setColspan(7);
        cell.setFixedHeight(3);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);       
     //7th ROW
         // column 1
        cell = new PdfPCell(font10Bold("No. Of Working Days"));
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 2
        cell = new PdfPCell(new Phrase(":"));
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 3
        cell = new PdfPCell(font10("30"));
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 4
        cell = new PdfPCell(new Phrase());
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 5
        cell = new PdfPCell(font10Bold("Leaves Taken"));
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // Coloum 6
        cell = new PdfPCell(new Phrase(":"));
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        //Column 7
        cell = new PdfPCell(font10(String.valueOf(info.getLeaves())));
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // spacing
        cell = new PdfPCell();
        cell.setColspan(7);
        cell.setFixedHeight(3);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
    //8TH ROW
         // column 1
        cell = new PdfPCell(font10Bold("ESIC No."));
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 2
        cell = new PdfPCell(new Phrase(":"));
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 3
        cell = new PdfPCell(font10(info.getEsi()));
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 4
        cell = new PdfPCell();
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // column 5
        cell = new PdfPCell(font10Bold("Advance Amount"));
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // Coloum 6
        cell = new PdfPCell(new Phrase(":"));
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        //Column 7
        cell = new PdfPCell(font10(String.valueOf(info.getSal_bean().getAdvance())));
        cell.setPaddingLeft(2);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        // spacing
        cell = new PdfPCell();
        cell.setColspan(7);
        cell.setFixedHeight(3);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
        //------------------------------------------------------------------------------------------
        // first nested table
        cell = new PdfPCell(innertable);
        cell.setCellEvent(roundRectangle);
        //cell.setBorder(Rectangle.NO_BORDER);-----------outer table of first table
        cell.setPadding(8);
        outertable.addCell(Headertable);
        outertable.addCell(cell);

        // inner table 2

        innertable = new PdfPTable(2);
        innertable.setWidths(new int[]{50,50});
        // first row
        // column 1
        
        cell = new PdfPCell(font12Bold("E A R N I N G S"));
       // cell.setBorder(Rectangle.NO_BORDER);
        cell.setMinimumHeight(20);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//        Image img = Image.getInstance(IMG1);
//        img.setWidthPercentage(10);
//        cell.addElement(img);
        innertable.addCell(cell); 
      
        // column 2
        cell = new PdfPCell(font12Bold("D E D U C T I O N S"));
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        innertable.addCell(cell);
       
        
        
   // Earnings Table
        PdfPTable  Earningtable = new PdfPTable(2);
        innertable.setWidths(new int[]{50,50});
        // first row
        // column 1
        cell = new PdfPCell(font10Bold("Basic Salary"));
        cell.setBorder(Rectangle.NO_BORDER);
        Earningtable.addCell(cell);
        // column 2
        cell = new PdfPCell(font10(String.valueOf(info.getSal_bean().getBasic())));
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setPaddingRight(20);
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        Earningtable.addCell(cell);
        // spacing
        cell = new PdfPCell(new Phrase());
        cell.setColspan(2);
        cell.setFixedHeight(3);
        cell.setBorder(Rectangle.NO_BORDER);
        Earningtable.addCell(cell);
     // Second row
        // column 1
        cell = new PdfPCell(font10Bold("House Rent Allowance"));
        cell.setBorder(Rectangle.NO_BORDER);
        Earningtable.addCell(cell);
        // column 2
        cell = new PdfPCell(font10(String.valueOf(info.getSal_bean().getHra())));
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setPaddingRight(20);
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        Earningtable.addCell(cell);
        // spacing
        cell = new PdfPCell(new Phrase());
        cell.setColspan(2);
        cell.setFixedHeight(3);
        cell.setBorder(Rectangle.NO_BORDER);
        Earningtable.addCell(cell);
      // Third row
        // column 1
        cell = new PdfPCell(font10Bold("Medical Allowance"));
        cell.setBorder(Rectangle.NO_BORDER);
        Earningtable.addCell(cell);
        // column 2
        cell = new PdfPCell(font10(String.valueOf(info.getSal_bean().getMedical())));
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setPaddingRight(20);
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        Earningtable.addCell(cell);
        // spacing
        cell = new PdfPCell(new Phrase());
        cell.setColspan(2);
        cell.setFixedHeight(3);
        cell.setBorder(Rectangle.NO_BORDER);
        Earningtable.addCell(cell);
        
    // fourth row
        // column 1
        cell = new PdfPCell(font10Bold("Conveyance Allowance"));
        cell.setBorder(Rectangle.NO_BORDER);
        Earningtable.addCell(cell);
        // column 2
        cell = new PdfPCell(font10(String.valueOf(info.getSal_bean().getConveyance())));
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setPaddingRight(20);
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        Earningtable.addCell(cell);
        // spacing
        cell = new PdfPCell(new Phrase());
        cell.setColspan(2);
        cell.setFixedHeight(3);
        cell.setBorder(Rectangle.NO_BORDER);
        Earningtable.addCell(cell);
        
        // Fifth row
        // column 1
        cell = new PdfPCell(font10Bold("Other Allowance"));
        cell.setBorder(Rectangle.NO_BORDER);
        Earningtable.addCell(cell);
        // column 2
        cell = new PdfPCell(font10(String.valueOf(info.getSal_bean().getSpecial()+info.getSal_bean().getOther())));
        cell.setPaddingRight(20);
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setBorder(Rectangle.NO_BORDER);
        Earningtable.addCell(cell);
        // spacing
        cell = new PdfPCell(new Phrase());
        cell.setColspan(2);
        cell.setFixedHeight(3);
        cell.setBorder(Rectangle.NO_BORDER);
        Earningtable.addCell(cell);
        
         // Sixth row
        // column 1
        cell = new PdfPCell(font10Bold("Special Allowance"));
        cell.setBorder(Rectangle.NO_BORDER);
        Earningtable.addCell(cell);
        // column 2
        cell = new PdfPCell(font10(String.valueOf(0.0)));
        cell.setPaddingRight(20);
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setBorder(Rectangle.NO_BORDER);
        Earningtable.addCell(cell);
        // spacing
        cell = new PdfPCell(new Phrase());
        cell.setColspan(2);
        cell.setFixedHeight(3);
        cell.setBorder(Rectangle.NO_BORDER);
        Earningtable.addCell(cell);
    // Deductions Table
        PdfPTable  Deductions = new PdfPTable(2);
        innertable.setWidths(new int[]{50,50});
        // first row
        // column 1
        cell = new PdfPCell(font10Bold("Income Tax"));
        cell.setBorder(Rectangle.NO_BORDER);
        Deductions.addCell(cell);
        // column 2
        cell = new PdfPCell(font10(String.valueOf(info.getSal_bean().getIncome_tax())));
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setPaddingRight(20);
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        Deductions.addCell(cell);
        // spacing
        cell = new PdfPCell(new Phrase());
        cell.setColspan(2);
        cell.setFixedHeight(3);
        cell.setBorder(Rectangle.NO_BORDER);
        Deductions.addCell(cell);
        
        // Second row
        // column 1
        cell = new PdfPCell(font10Bold("Profession Tax"));
        cell.setBorder(Rectangle.NO_BORDER);
        Deductions.addCell(cell);
        // column 2
        cell = new PdfPCell(font10(String.valueOf(info.getSal_bean().getProfessional_tax())));
        cell.setPaddingRight(20);
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setBorder(Rectangle.NO_BORDER);
        Deductions.addCell(cell);
        // spacing
        cell = new PdfPCell(new Phrase());
        cell.setColspan(2);
        cell.setFixedHeight(3);
        cell.setBorder(Rectangle.NO_BORDER);
        Deductions.addCell(cell);
        
         // Third Row
        // column 1
        cell = new PdfPCell(font10Bold("EPF "));
        cell.setBorder(Rectangle.NO_BORDER);
        Deductions.addCell(cell);
        // column 2
        cell = new PdfPCell(font10(String.valueOf(info.getSal_bean().getEpf_amount())));
        cell.setPaddingRight(20);
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setBorder(Rectangle.NO_BORDER);
        Deductions.addCell(cell);
        // spacing
        cell = new PdfPCell(new Phrase());
        cell.setColspan(2);
        cell.setFixedHeight(3);
        cell.setBorder(Rectangle.NO_BORDER);
        Deductions.addCell(cell);
        
         // Fourth Row
        // column 1
        cell = new PdfPCell(font10Bold("ESI "));
        cell.setBorder(Rectangle.NO_BORDER);
        Deductions.addCell(cell);
        // column 2
        cell = new PdfPCell(font10(String.valueOf(info.getSal_bean().getEsi_amount())));
        cell.setPaddingRight(20);
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setBorder(Rectangle.NO_BORDER);
        Deductions.addCell(cell);
        // spacing
        cell = new PdfPCell(new Phrase());
        cell.setColspan(2);
        cell.setFixedHeight(3);
        cell.setBorder(Rectangle.NO_BORDER);
        Deductions.addCell(cell);
        
         // fifth Row
        // column 1
        cell = new PdfPCell(font10Bold("LWP "));
        cell.setBorder(Rectangle.NO_BORDER);
        Deductions.addCell(cell);
        // column 2
        cell = new PdfPCell(font10(String.valueOf(info.getSal_bean().getLwp())));
        cell.setPaddingRight(20);
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setBorder(Rectangle.NO_BORDER);
        Deductions.addCell(cell);
        // spacing
        cell = new PdfPCell(new Phrase());
        cell.setColspan(2);
        cell.setFixedHeight(3);
        cell.setBorder(Rectangle.NO_BORDER);
        Deductions.addCell(cell);
        
        //identifying the advance amount and added to other deductions and mention it is a advance amount
        String adv="";
        double other_ded=0;
        if(info.getSal_bean().getAdvance()>0)
        {
            adv="( Adv )";
            other_ded=info.getSal_bean().getAdvance();
        }
        else
        {
            other_ded=info.getSal_bean().getOther_dedu();
        }
        // Sixth Row
        // column 1
        cell = new PdfPCell(font10Bold("Other Deductions "+adv));
        cell.setBorder(Rectangle.NO_BORDER);
        Deductions.addCell(cell);
        // column 2
        cell = new PdfPCell(font10(String.valueOf(other_ded)));
        cell.setPaddingRight(20);
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setBorder(Rectangle.NO_BORDER);
        Deductions.addCell(cell);
        // spacing
        cell = new PdfPCell(new Phrase());
        cell.setColspan(2);
        cell.setFixedHeight(3);
        cell.setBorder(Rectangle.NO_BORDER);
        Deductions.addCell(cell);
      // Gross Total Table
        PdfPTable  Total = new PdfPTable(2);
        innertable.setWidths(new int[]{50,50});
        // first row
        // column 1
        cell = new PdfPCell(font10Bold("CTC"));
        cell.setBorder(Rectangle.NO_BORDER);
        Total.addCell(cell);
        // column 2
        cell = new PdfPCell(font12Bold(String.valueOf(info.getSal_bean().getGross())));
        cell.setPaddingRight(20);
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setBorder(Rectangle.NO_BORDER);
        Total.addCell(cell);
        // spacing
        cell = new PdfPCell(new Phrase());
        cell.setColspan(2);
        cell.setFixedHeight(3);
        cell.setBorder(Rectangle.NO_BORDER);
        Total.addCell(cell);
        
         // Deduction Total Table
        PdfPTable  DedTotal = new PdfPTable(2);
        innertable.setWidths(new int[]{50,50});
        // first row
        // column 1
        cell = new PdfPCell(font10Bold("Total Deduction"));
        cell.setBorder(Rectangle.NO_BORDER);
        DedTotal.addCell(cell);
        // column 2
        cell = new PdfPCell(font12Bold(String.valueOf(info.getSal_bean().getGross()-info.getSal_bean().getNet())));
        cell.setPaddingRight(20);
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setBorder(Rectangle.NO_BORDER);
        DedTotal.addCell(cell);
        // spacing
        cell = new PdfPCell(new Phrase());
        cell.setColspan(2);
        cell.setFixedHeight(3);
        cell.setBorder(Rectangle.NO_BORDER);
        DedTotal.addCell(cell);
        
        innertable.addCell(Earningtable);
        innertable.addCell(Deductions);
        innertable.addCell(Total);
        innertable.addCell(DedTotal);
        
        // spacing
        cell = new PdfPCell(new Phrase("Space"));
        cell.setColspan(4);
        cell.setFixedHeight(3);
        cell.setBorder(Rectangle.NO_BORDER);
        innertable.addCell(cell);
    
        // second nested table
        cell = new PdfPCell(innertable);
        cell.setCellEvent(roundRectangle);
        //cell.setBorder(Rectangle.NO_BORDER);
        cell.setPadding(8);
        outertable.addCell(cell);
        // add the table
        
        
        PdfPTable outtable = new PdfPTable(4);
        outtable.setWidths(new int[]{13,3,17,90});
        outtable.setWidthPercentage(90);
        outtable.setSpacingBefore(20f);
        
        // first row
        // column 1
        cell = new PdfPCell(font12Bold("Net Pay :"));
        cell.setPaddingRight(5);
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
       cell.setBorder(Rectangle.NO_BORDER);
        outtable.addCell(cell);
        
        //column 2
         outtable.addCell(RupeeImageCell(IMG1));
         
        // column 3  ;
        Chunk chunk1 = new Chunk(String.valueOf(info.getSal_bean().getNet()));
        chunk1.setUnderline(1.5f, -4);
        cell = new PdfPCell(new Phrase(chunk1)); 
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        cell.setBorder(Rectangle.NO_BORDER);
        outtable.addCell(cell);
        
        // column 4
        //cell = new PdfPCell(font8Bold("( Seventy Seven Lakh+s Seventy Seven Thousand Seven Hundred Seventy Seven )"));
        String text = originalNumToLetter.toLetter(Integer.toString((int)info.getSal_bean().getNet()));
        cell = new PdfPCell(font8Bold("( "+text+" Only )"));
        cell.setPaddingRight(5);
        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
        cell.setBorder(Rectangle.NO_BORDER);
        outtable.addCell(cell);
        // spacing
        cell = new PdfPCell();
        cell.setColspan(4);
        cell.setFixedHeight(3);
        cell.setBorder(Rectangle.NO_BORDER);
        outtable.addCell(cell);
        
        document.add(outertable);
        document.add(outtable);
        Paragraph dummy = new Paragraph("\u00a0");
        Paragraph dummy2 = new Paragraph("");
       // document.add(dummy);
       // document.add(Phrase.getInstance(2, "           In Words : Rupess Sixty seven thousand two hundred sixty Three only ", new Font(Font.FontFamily.TIMES_ROMAN, 12))); 
        document.add(dummy2);
        document.add(Phrase.getInstance(2, "                 Note : Since this is a computer-generated statement, it does not need any signature.", new Font(Font.FontFamily.TIMES_ROMAN, 8)));
        document.close();
    }
    
     public static Phrase font8Bold( String phr)
    {
        Phrase pr = new Phrase(Phrase.getInstance(0, phr, new Font(Font.FontFamily.TIMES_ROMAN, 9,Font.BOLD)));
        return pr;
    }
    public static Phrase font10( String phr)
    {
        
        Phrase pr = new Phrase(Phrase.getInstance(10, phr, new Font(Font.FontFamily.TIMES_ROMAN, 10)));
        
        return pr;
    }
     public static Phrase font10Bold( String phr)
    {
        Phrase pr = new Phrase(Phrase.getInstance(0, phr, new Font(Font.FontFamily.TIMES_ROMAN, 10,Font.BOLD)));
        return pr;
    }
      public static Paragraph font10R( String phr)
    {
        
       Paragraph info = new Paragraph(phr);
       info.setAlignment(Element.ALIGN_CENTER);      
        return info;
    }
     
     public static Phrase font12Bold( String phr)
    {
        Phrase pr = new Phrase(Phrase.getInstance(0, phr, new Font(Font.FontFamily.TIMES_ROMAN, 12,Font.BOLD)));
        return pr;
    }
     
      public static PdfPCell createImageCell(String path) throws DocumentException, IOException 
      {
        Image img = Image.getInstance(path);
       // img.setWidthPercentage(5);
        PdfPCell cell = new PdfPCell(img, true);
        cell.setFixedHeight(50);
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setPaddingLeft(2);
        return cell;
      }
      public static PdfPCell RupeeImageCell(String path) throws DocumentException, IOException 
      {
        Image img = Image.getInstance(path);       
        PdfPCell cell = new PdfPCell(img, true);
        cell.setFixedHeight(5);
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);       
        cell.setPaddingTop(3);
        return cell;
      }
}