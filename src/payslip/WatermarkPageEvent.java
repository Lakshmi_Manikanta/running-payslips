/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package payslip;

import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.GrayColor;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfGState;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;

public class WatermarkPageEvent extends PdfPageEventHelper {

    Font FONT = new Font(Font.FontFamily.HELVETICA, 52, Font.BOLD, new GrayColor(0.85f));

    @Override
    public void onEndPage(PdfWriter writer, Document document) 
    { try
      {
          PdfContentByte over =writer.getDirectContentUnder();
        if(false)
        {
        ColumnText.showTextAligned(over,
                Element.ALIGN_CENTER, new Phrase("Bloom Solutions", FONT),
                297.5f, 421, writer.getPageNumber() % 2 == 1 ? 45 : -45);
        }
        else {
            
               PdfGState gs1 = new PdfGState();
               gs1.setFillOpacity(0.3f);
               over.setGState(gs1);
               
               Image img = Image.getInstance("D:\\F drive BK\\getpic.png");
               float w = img.getScaledWidth();
               float h = img.getScaledHeight();
               over.addImage(img, w+60, 0, 0, h+60, 291 - (w / 2), 426 - (h / 80));
               //System .out.println((291 - (w / 2))+" ,"+( 426 - (0)));
                
            }
      }
         catch(Exception e)
            {
                e.printStackTrace();
            }
    }
}