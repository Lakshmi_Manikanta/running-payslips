package payslip;


import employee_bean.Information_Bean;
import java.io.*;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import javax.mail.*;
 import javax.mail.internet.*;
 import javax.activation.*;
 import java.util.Properties;

public class Mailer {
    
    Session session;
    Properties props = new Properties();
    InputStream configfile = null;
    
    public static void main(String[] args) throws Exception
    {
        Mailer.upDateRequire();
        
    }
                          
public Session mailSession() throws Exception
{        final String fromf= "bloomsolutions.pvt.ld@gmail.com";  //Trail      
         final String pwdf= "bloomsolutions@123";                       
            props.load(Mailer.class.getClassLoader().getResourceAsStream("Mail_config.properties"));       
            System.getProperties().put("http.proxyHost","192.168.32.54");
            System.getProperties().put("http.proxyPort","80");
            session = Session.getDefaultInstance(props,  
             new javax.mail.Authenticator() {  
             protected PasswordAuthentication getPasswordAuthentication() {  
             return new PasswordAuthentication(fromf,pwdf);  
   }  
  });
    //System.out.println("Mail Session "+session);
    return session;
}
  public synchronized static String sendTO ( Information_Bean bn, String path)throws MessagingException,Exception
     {               
  try {       
   MimeMessage message = new MimeMessage(new Mailer().mailSession());
     Multipart multipart = new MimeMultipart();  
     String mailid=bn.getE_mail();
   message.setFrom(new InternetAddress("bloomsolutions.pvt.ld@gmail.com"));  
   if(bn.getE_mail().equals("")||bn.getE_mail().equals("null")||bn.getE_mail()==null ||bn.getE_mail().length()<3)      
         {
             mailid="bloom.kameswararao@gmail.com";
             message.setSubject("Sir, This "+bn.getE_code()+" Employee not Given ");              
         }
   else{
       message.setSubject("Pay Slip for the Month of "+bn.getSal_bean().getMonth()+"-"+bn.getSal_bean().getYear());   
       //message.setSubject("Testing... Payslip "); 
   }
   //mailid="lakshmimanikanta26@gmail.com";
   message.addRecipient(Message.RecipientType.TO,new InternetAddress(mailid)); 
   BodyPart messageBodyPart1 = new MimeBodyPart();    
  messageBodyPart1.setText("This is your Computer generated Pay Slip for the "+bn.getSal_bean().getMonth()+"-"+bn.getSal_bean().getYear());  
 // String htmlText = new ReadHtml().readEmailFromHtml("C:\\Users\\Lakshmi Manikanta\\Desktop\\Ht.html",Html_data);
//  String htmlText = new ReadHtml().readEmailFromHtml("Email_temp.html",Html_data);
//  messageBodyPart1.setContent(htmlText, "text/html");
   MimeBodyPart messageBodyPart2 = new MimeBodyPart();  
   String f= path;  
   //System.out.println(f);
   DataSource source = new FileDataSource(f);
   // System.out.println("file found ");
    messageBodyPart2.setDataHandler(new DataHandler(source));
    messageBodyPart2.setFileName(bn.getE_code()+"-"+bn.getSal_bean().getMonth()+"-"+bn.getSal_bean().getYear()+".pdf");           
    multipart.addBodyPart(messageBodyPart1);  
    multipart.addBodyPart(messageBodyPart2);   
    message.setContent(multipart );   
    Transport.send(message);  
   System.out.print(bn.getE_code()+" his mail Id:"+mailid);
   System.out.println("message sent to"+"\t"+bn.getE_code()+" successfully");  
   
  } catch (MessagingException e) {throw new RuntimeException(e);}  
   return "m";
 } 
  public static void upDateRequire() throws Exception
  {
      Properties prop =new Properties();
      prop.load(Mailer.class.getClassLoader().getResourceAsStream("Mail_config.properties"));
      MimeMessage message = new MimeMessage(new Mailer().mailSession());   
      Multipart multipart = new MimeMultipart();       
      message.setFrom(new InternetAddress("bloomsolutions.pvt.ld@gmail.com"));  
      InternetAddress[] Toadds= InternetAddress.parse(prop.getProperty("toaddress"));
      InternetAddress[] CCadds= InternetAddress.parse(prop.getProperty("ccaddress"));
      message.addRecipients(Message.RecipientType.TO,Toadds);
      message.setRecipients(Message.RecipientType.CC,CCadds);
      message.setSubject("Employess monthly salary Data require for table updation");
      BodyPart messagePart = new MimeBodyPart();
      File file = new File(ClassLoader.getSystemResource("Salary_Sheet.xlsx").t‌​oURI());
      MimeBodyPart messagePart2 = new MimeBodyPart(); 
      messagePart.setText("Dear Sir/Madam"+"\n"+"\n"+"\t"+"it is required to updation in Database table for generating pay slips so we need Employee salary sheet, fill the attached one and submit as Quickly ");
      DataSource source = new FileDataSource(file);
      messagePart2.setDataHandler(new DataHandler(source));
      messagePart2.setFileName("Salary_Sheet.xlsx");
      multipart.addBodyPart(messagePart);
      multipart.addBodyPart(messagePart2);
      message.setContent(multipart );
      Transport.send(message);          
  }//update Reminder
  public static void exceptionMail(Exception e) throws Exception
  {
      Properties prop =new Properties();
      prop.load(Mailer.class.getClassLoader().getResourceAsStream("Mail_config.properties"));
      MimeMessage message = new MimeMessage(new Mailer().mailSession());   
      Multipart multipart = new MimeMultipart();       
      message.setFrom(new InternetAddress("bloomsolutions.pvt.ld@gmail.com"));  
      InternetAddress[] Erradds= InternetAddress.parse(prop.getProperty("ErrorAddress"));    
      message.addRecipients(Message.RecipientType.TO,Erradds);   
      message.setSubject("Exception Raised ");
      BodyPart messagePart = new MimeBodyPart();     
      //MimeBodyPart messagePart2 = new MimeBodyPart(); 
      StringWriter writer = new StringWriter();
      PrintWriter pWriter = new PrintWriter(writer);
      e.printStackTrace(pWriter);     
      messagePart.setText(writer.toString());    
      multipart.addBodyPart(messagePart);
      message.setContent(multipart );
      Transport.send(message); 
      System.out.println("Exception Mailed");   
  }
  public static void reportMail() throws Exception
  {
      Properties prop =new Properties();
      prop.load(Mailer.class.getClassLoader().getResourceAsStream("Mail_config.properties"));
      MimeMessage message = new MimeMessage(new Mailer().mailSession());   
      Multipart multipart = new MimeMultipart();       
      message.setFrom(new InternetAddress("bloomsolutions.pvt.ld@gmail.com"));  
      InternetAddress[] Erradds= InternetAddress.parse(prop.getProperty("ErrorAddress"));    
      message.addRecipients(Message.RecipientType.TO,Erradds);   
      message.setSubject("Daily status Report ");
      BodyPart messagePart = new MimeBodyPart();     
      //MimeBodyPart messagePart2 = new MimeBodyPart();      
//      messagePart.setText("Date : "+new SimpleDateFormat().format(new Date())+"\n"+"Data Base Connection : "+DBConnection.getConnection()+"\n"+"Mail connection : "+new Mailer().mailSession());    
      multipart.addBodyPart(messagePart);
      message.setContent(multipart );
      Transport.send(message); 
      System.out.println("Daily Status Mailed");   
  }
 }//Mailer
